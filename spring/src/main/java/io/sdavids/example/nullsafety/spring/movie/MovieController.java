/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.nullsafety.spring.movie;

import static java.util.Objects.requireNonNull;

import io.sdavids.example.nullsafety.spring.movie.domain.MovieFacade;
import io.sdavids.example.nullsafety.spring.movie.dto.MovieDto;
import java.util.NoSuchElementException;
import org.springframework.lang.NonNull;

public class MovieController {

  private final MovieFacade movieFacade;

  MovieController(@NonNull MovieFacade movieFacade) { // <1>
    this.movieFacade = requireNonNull(movieFacade, "movieFacade");
  }

  public MovieDto getMovie(String title) {
    requireNonNull(title, "title");
    return movieFacade.show(title).orElseThrow(() -> new NoSuchElementException(title));
  }

  public void postMovie(MovieDto movieDto) {
    movieFacade.add(movieDto);
  }
}
